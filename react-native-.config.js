module.exports = {
  dependencies: {
    'appcenter': {
      platforms: {
        android: null
      }
    },
    'appcenter-crashes': {
      platforms: {
        android: null
      }
    },
    'appcenter-analytics': {
      platforms: {
        android: null
      }
    },
    'react-native-code-push': {
      platforms: {
        android: null
      }
    },
    'react-native-firebase': {
      platforms: {
        android: undefined
      }
    }
  }
}