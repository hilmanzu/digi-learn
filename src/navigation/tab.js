import * as React from 'react';
import { View, StyleSheet, Dimensions,Image,Text } from 'react-native';
import { TabView, SceneMap,TabBar } from 'react-native-tab-view';
//transaksi
import Checkin from '../screen/checkin'
import Checkout from '../screen/checkout'
import take from '../screen/take'
// import take_out from '../screen/take_out'

const FirstRoute = () => (
  <Checkin/>
);

const SecondRoute = () => (
  <Checkout/>
);

export default class TabViewExample extends React.Component {

  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'Checkin' },
      { key: 'second', title: 'Checkout' },
    ],
  };

  renderTabBar(props) {
    return (
      <TabBar
        style={{backgroundColor: '#420000', elevation: 0, borderColor: '#000000', borderBottomWidth: 1, height:50}}
        labelStyle={{color: '#fff', fontSize: 12, fontWeight: 'bold'}}
        {...props}
        indicatorStyle={{backgroundColor: '#00818A', height: 3}}
      />
    );
  }

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({
          first: FirstRoute,
          second: SecondRoute,
        })}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{ width: Dimensions.get('window').width }}
        renderTabBar={this.renderTabBar}
        renderHeader={this.renderHeader}
      />
    );
  }
}