import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { createMaterialTopTabNavigator,createSwitchNavigator, createStackNavigator, createAppContainer,createBottomTabNavigator } from 'react-navigation';
///opening
import Splashscreen from '../screen/splashscreen'

///auth
import login from '../screen/login'
import register from '../screen/register'
///menu
import menu from '../screen/home'
import profile from '../screen/profile'
///checkin
import presensi from '../navigation/tab'
import take from '../screen/take'
import report from '../screen/report'
///reimburse
import reimburse from '../screen/reimburse'
import take_reimburse from '../screen/take_reimburse'
///perizinan
import perizinan from '../screen/perizinan'
import take_perizinan from '../screen/take_perizinan'
import news from '../screen/news'
///chat
import groub from '../screen/group'
import chat from '../screen/chat'
import anggota from '../screen/anggota'
import web from '../screen/web'

// const Tab = createMaterialTopTabNavigator({
//   checkin          : checkin,
//   checkout         : checkout,
// },{
//   navigationOptions:{ header:{ visible:true }},
//     lazy:false,
//     tabBarPosition: 'bottom',
//     animationEnabled: true,
//     swipeEnabled: true,
//     tabBarTextFontFamily: 'quicksand',
//     tabBarOptions: {
//       activeTintColor: '#fff',
//       inactiveTintColor: '#000000',
//       showIcon: false,
//       showLabel: true,
//       style:{height: 50,fontWeight:'bold'}
//     },
// })

// const Absensi = createStackNavigator({
//   subabsen      : subabsen,
// },{headerMode   : 'none'});

const webview = createStackNavigator({
  news      : news
},{headerMode   : 'none'});

const Auth = createStackNavigator({
  login         : login,
  register      : register
},{headerMode   : 'none'})

const Opening = createStackNavigator({
  Splash  : Splashscreen
},{headerMode   : 'none'})

const Base = createStackNavigator({
  menu            : menu,
  profile         : profile,
  presensi        : presensi,
  take            : take,
  reimburse       : reimburse,
  take_reimburse  : take_reimburse,
  perizinan       : perizinan,
  take_perizinan  : take_perizinan,
  news            : news,
  groub           : groub,
  chat            : chat,
  anggota         : anggota,
  web             : web,
  report          : report
},{headerMode   : 'none'})



export default createAppContainer(createSwitchNavigator(
  {
    Opening   : Opening,
    Auth 		  : Auth,
    Base 		  : Base 
  },
  {
    initialRouteName: 'Opening',
  }
))

