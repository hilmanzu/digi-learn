import React, { Component } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  View,
  Alert,
  ActivityIndicator,
  Modal,
  Picker
} from "react-native";
import {Card} from "native-base"
import firebase from "react-native-firebase"
import store from 'react-native-simple-store';

class Register extends Component {

   constructor(props){
      super(props);
      this.state = {
        email:'',
        password:'',
        nama:'',
        loading:'',
        company:'',
        view:false,
        dashboard:[],
        isModalVisible:false,
        hide:true
      }
    }

  componentDidMount(){
      const db = firebase.firestore();
      const doc = db.collection("master").doc("ukkWRHGNcpXC0kDOYFqF").collection("category_absen")
      doc.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          dashboard : data
        })
    })
  }

  render() {
  const {dashboard} = this.state
    return (
        <View style={styles.Content}>
          <Image source={{uri : "https://i.ibb.co/b7Yj3vH/presensi.png"}} style={styles.logo} />
          <Card style={styles.card}>
            <Text style={styles.text1}>Form Pendaftaran</Text>

            <TextInput 
              placeholder={'email'} 
              style={styles.textinput1} 
              onChangeText={(email)=> this.setState({email})}
              returnKeyType='next'/>

            <TextInput 
              placeholder={'Nama Anda'} 
              style={styles.textinput2} 
              onChangeText={(nama)=> this.setState({nama})}
              returnKeyType='next'/>

            <View style={{alignItems:'center',justifyContent:'center',marginTop:20}}>
              <View style={{width: 250,height: 37,alignItems:'center',justifyContent:'center',backgroundColor: `rgba(255, 255, 255, 1.0)`,borderRadius:30}}>
                <Picker
                  selectedValue={this.state.company}
                  style={{width: 220,height: 37}}
                  onValueChange={(company) => this.setState({company})}
                  itemStyle={{height:37,width:250}} >
                  <Picker.Item label='Pilih Company' value={''}/>
                      {dashboard.map((item, index) => {
                      return (<Picker.Item label={item.data.nama} value={item.data.nama} key={item.id}/>) 
                      })}
                </Picker>
              </View>
            </View>

            <TextInput 
              placeholder={'password'} 
              style={styles.texinput3} 
              secureTextEntry={this.state.hide}
              onChangeText={(password)=> this.setState({password})}
              returnKeyType='next'/>

            <TouchableOpacity onPressOut={()=> this.setState({hide:true})} onPressIn={()=> this.setState({hide:false})}>
              <Text style={{color: `rgba(34, 25, 77, 0.44)`,fontSize: 12,marginLeft: 45,marginTop: 15}}>Lihat Password</Text>
            </TouchableOpacity>


            <TouchableOpacity onPress={this.register}>
              <Card style={styles.bottondaftar}>
                <Text style={styles.text3}>Daftar</Text>
              </Card>
            </TouchableOpacity>

          </Card>
          <Modal visible={this.state.view} transparent={true} >
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <ActivityIndicator
                  color={'#149BE5'}
                  size={`large`}
                  style={styles.s34e24b9b}
                />
            </View>
          </Modal>
        </View>
    );
  }

  register=()=>{
    var text = this.state.password
     if (this.state.email == ''){
      alert('Isi email anda')
    }else if (this.state.nama == ''){
      alert('Isi Nama anda')
    }else if (this.state.company == ''){
      alert('Pilih Company Anda')
    }else if (text.length <= 7){
      alert('Password harus lebih 7 karakter')
    }else{
      this.setState({ view: true });
      const { email, password } = this.state;
      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then((response) => {
          if (response.user.uid){
            this.setState({ view: false });
            const db = firebase.firestore()
            db.collection("user").doc(response.user.uid).set({
              email        : this.state.email,
              nama         : this.state.nama,
              company      : this.state.company,
              password     : this.state.password
            })
            store.save('uid', response.user.uid)
            this.props.navigation.navigate('Splashscreen');
          }
        })

        .catch((error)=>{
          if ( error.code == 'auth/email-already-in-use' ){
          this.setState({ view: false });
          Alert.alert('Akun Telah Digunakan','Silahkan daftar dengan email lain')
          }else if ( error.code == 'auth/user-not-found' ){
          this.setState({ view: false });
          Alert.alert('Akun Tidak ditemukan','Silahkan daftar dengan email lain')
          }else if ( error.code == 'auth/wrong-password'){
          this.setState({ view: false });
          Alert.alert('password anda salah',)
          }
        })
    }
  }

}

export default Register;

const styles = StyleSheet.create({
  logo: { height: 80, marginLeft: 140, marginRight: 140, width: 120 },
  text1: {
    color: '#149BE5',
    fontSize: 18,
    marginLeft: 35,
    marginTop: 35,
    fontWeight: `bold`,
  },
  textinput1: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 44,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingTop: 10,
    width: 250
  },
  textinput2: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 16,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingTop: 10,
    width: 250
  },
  texinput3: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 16,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingTop: 10,
    width: 250
  },
  texinput4: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius:30,
    height:37,
    width:250,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 16,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingTop: 10,
    justifyContent:'center',
    borderStyle:'dashed'
  },
  text3: {
    color: `rgba(255, 255, 255, 1)`,
    fontSize: 18,
    fontWeight: `bold`
  },
  bottondaftar: {
    alignItems: `center`,
    backgroundColor: '#149BE5',
    borderRadius: 30,
    height: 37,
    justifyContent: `center`,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 10,
    width: 250
  },
  text2: {
    color: `rgba(170, 170, 170, 1)`,
    fontSize: 12,
    marginLeft: 29,
    marginTop: 23
  },
  card: {
    backgroundColor: `rgba(247, 247, 247, 1)`,
    borderRadius: 10,
    height: 400,
    marginTop: 35,
    width: 300
  },
  Content: {
    alignItems: `center`,
    backgroundColor: `rgba(255, 255, 255, 1)`,
    flex: 1,
    justifyContent: `center`
  }
});