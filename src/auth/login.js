import React, { Fragment } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  View,
  Alert,
  ActivityIndicator,
  Modal
} from "react-native";
import {Card} from "native-base"
import firebase from "react-native-firebase"
import store from 'react-native-simple-store';


class Login extends React.PureComponent {   

  constructor(props){
    super(props);
    this.state = {
      email:'',
      password:'',
      loading: true,
      key:true,
      view:false,
      hide:true
    }
  }

 forgotPassword = () => {

    if (this.state.email === '') {
      Alert.alert('Opss','Harap isi email anda')
    } else {
      firebase.auth().sendPasswordResetEmail(this.state.email)
        .then(function (user) {
          Alert.alert('Hai','Perubahan password sudah dikirim di email anda, harap cek email anda sekarang')
        }).catch(function (e) {
          alert('Terjadi kesalahan harap periksa koneksi')
        })
    }
  }


  render() {
    return (
      <Fragment>
        <View style={styles.Content}>
          <Image source={{uri : "https://i.ibb.co/b7Yj3vH/presensi.png"}} style={styles.logo} />
          <Card style={styles.card}>
            <Text style={styles.text1}>
              Presensi Online
            </Text>
            <Text style={styles.text2}>
              Aplikasi Absensi Bisnis Terpercaya Se-Indonesia
            </Text>

            <TextInput placeholder={`email`} style={styles.textinput1} onChangeText={(email)=> this.setState({email})}/>
            <TextInput secureTextEntry={this.state.hide} placeholder={`Password`} style={styles.textinput} onChangeText={(password)=> this.setState({password})}/>
            
            <TouchableOpacity onPressOut={this.forgotPassword} onPressIn={()=> this.setState({hide:false})}>
              <Text style={styles.text3}>Reset password disini</Text>
            </TouchableOpacity>

            <TouchableOpacity  onPress={this.onLogin}>
              <Card style={styles.masuk}>
                  <Text style={styles.bottonmasuk}>Masuk</Text>
              </Card>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('register')}>
              <Card style={styles.masuk}>
                <Text style={styles.bottonmasuk}>Daftar</Text>
              </Card>
            </TouchableOpacity>
          </Card>
        </View>
        <Modal visible={this.state.view} transparent={true} >
          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <ActivityIndicator
                color={'#149BE5'}
                size={`large`}
                style={styles.s34e24b9b}
              />
          </View>
        </Modal>
      </Fragment>
    );
  }
  
  onLogin = () => {
    if (this.state.email == ''){
      alert('Isi email anda')
    }else if (this.state.password == ''){
      alert('Isi Password anda')
    }else{
      this.setState({ view: true });
      const { email, password } = this.state;
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then((responseJson) => {
          if (responseJson.user.uid){
          store.save('uid', responseJson.user.uid)
          this.props.navigation.navigate('main')
          this.setState({ view: false });
          Alert.alert(
            'Selamat Datang',
            'Presensi Online'
          )
          }
        })
        .catch((error) => {
          if ( error.code == 'auth/user-not-found' ){
          Alert.alert('Akun Tidak ditemukan','Silahkan register untuk membuat akun')
          }else if ( error.code == 'auth/wrong-password'){
          Alert.alert('password anda salah','silahkan periksa kembali password anda')
          }
         this.setState({ view: false });
        });
      }
    }
}

export default Login;

const styles = StyleSheet.create({
  logo: { height: 80, marginLeft: 140, marginRight: 140, width: 120 },
  text1: {
    color: '#149BE5',
    fontSize: 18,
    fontWeight: `bold`,
    marginLeft: 32,
    marginRight: 95,
    marginTop: 25
  },
  text2: {
    color: `rgba(170, 170, 170, 1)`,
    fontSize: 12,
    marginLeft: 32,
    marginRight: 85,
    marginTop: 15
  },
  textinput1: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 17,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 10,
    width: 250
  },
  textinput: {
    backgroundColor: `rgba(255, 255, 255, 1.0)`,
    borderRadius: 30,
    height: 37,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 17,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 10,
    width: 250
  },
  text3: {
    color: `rgba(34, 25, 77, 0.44)`,
    fontSize: 12,
    marginLeft: 45,
    marginTop: 15
  },
  bottonmasuk: { color: `rgba(255, 255, 255, 1)`, fontWeight: `bold` },
  masuk: {
    alignItems: `center`,
    backgroundColor: '#149BE5',
    borderRadius: 30,
    height: 37,
    justifyContent: `center`,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 15,
    width: 250
  },
  text4: { color: '#149BE5', textAlign: `center` },
  buttondaftar: {
    alignItems: `center`,
    justifyContent: `center`,
    marginLeft: 90,
    marginRight: 90,
    marginTop: 23
  },
  card: {
    backgroundColor: `rgba(247, 247, 247, 1)`,
    borderRadius: 10,
    height: 380,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 35,
    width: 300
  },
  Content: {
    alignItems: `center`,
    backgroundColor: `rgba(255, 255, 255, 1)`,
    flex: 1,
    justifyContent: `center`
  }
});