import React, {Component} from 'react';
import {ActivityIndicator,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from './component/style'
import store from 'react-native-simple-store';
import moment from 'moment'
import 'moment/locale/id'
moment.locale('id')
import firebase from "react-native-firebase"
import renderIf from './component/renderIf'
const haversine = require('haversine')

export default class menu extends Component{

   static navigationOptions = {
    headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
      tabBarLabel: 'Rangking',
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={{uri:'https://i.ibb.co/Vxm6NxJ/003-tablet.png'}}
          style={[{width:20,height:20}, { tintColor: tintColor }]}
        />
      ),
    };

  constructor(props){
    super(props)
    this.state = {
      dashboard:[],
      jarak : [],
      loading:true,
      company:''
      }
    }

  componentWillMount(){
    store.get('profile')
      .then((res)=>{

      const db = firebase.firestore();
      const docref = db.collection("all_absensi").orderBy('waktu', 'asc')
      docref.onSnapshot(async(querySnapshot)=>{
          var data = []
            querySnapshot.forEach((doc)=>{
              let item = data
              item.push({
                data : doc.data(),
                id   : doc.id
              })
            })
          this.setState({
            dashboard : data,loading:false,company:res.company
          })
        })

    })
  }

  render() {
  const { dashboard,time,jarak,loading } = this.state 
  const filter = dashboard.filter((item) => item.data.company == this.state.company && moment(item.data.waktu.toDate()).format('MMM Do YY') === moment().format('MMM Do YY'))
  
  const Banner = firebase.admob.Banner;
  const AdRequest = firebase.admob.AdRequest;
  const request = new AdRequest();
  request.addKeyword('laptop murah');

  if (loading === true) {
      return(
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.headertext}>Absensi Digital Finger</Text>
          </View>
          <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
            <Text style={{color:'#83142c',marginBottom:20}}>Sedang mengambil data</Text>
            <ActivityIndicator
              color={`rgba(0, 163, 192, 1)`}
              size={`large`}
              style={styles.spin}
            />
          </View>
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Peringkat Hari ini</Text>
          <Text style={{flex:1,textAlign:'right',marginRight:20,color:'#fff'}}>{moment().format("Do MMMM YYYY")}</Text>
        </View>
        <View style={{width:360,height:30,elevation:5,backgroundColor:'#fff',alignItems:'center',flexDirection:'row'}}>
          <Text style={{flex:1,marginLeft:20}}>Nama</Text>
          <Text style={{flex:1,textAlign:'right',marginRight:20}}>Waktu</Text>
        </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center'}}>
            {filter.map((data)=>
              <View style={{width:360,height:50,backgroundColor:'#fff',alignItems:'center',flexDirection:'row',}}>
                <Text style={{flex:1,marginLeft:20}}>{data.data.nama}</Text>
                <Text style={{flex:1,textAlign:'right',marginRight:20}}>{moment(data.data.waktu.toDate()).format('LT')}</Text>
              </View>
            )}
          </ScrollView>
        <Banner
          unitId={'ca-app-pub-8694320270975969/7056513772'}
          size={'SMART_BANNER'}
          request={request.build()}
          onAdLoaded={() => {
            console.log('Advert loaded');
          }}
        />
      </View>
    );
  }

}
