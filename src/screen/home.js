import React, {Component} from 'react';
import {Modal,BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from "../component/style/home"
import store from 'react-native-simple-store';
import firebase from "react-native-firebase"
import renderIf from "../component/renderIf"

export default class home extends Component{

  constructor(props) {
    super(props)
      this.state = {
        nama: '',
        image:'https://www.fote.org.uk/wp-content/uploads/2017/03/profile-icon-300x300.png',
        news:[],
        pengumuman:[],
        role:'',
      }
  }

  componentDidMount(){

    store.get('nama').then((res)=>{
        this.setState({ nama:res})
    })

    store.get('image').then((res)=>{
        this.setState({ image:res})
    })

    store.get('role').then((res)=>{
        this.setState({ role:res})
    })

    store.get('id_perusahaan').then((data)=>{
      const db = firebase.firestore();
      const news = db.collection("perusahaan").doc(data).collection("pengumuman").orderBy('waktu', 'desc')
      news.onSnapshot(async(querySnapshot)=>{
          var data = []
            querySnapshot.forEach((doc)=>{
              let item = data
              item.push({
                data   : doc.data(),
                id     : doc.id,
                number : data.length + 1
              })
            })
          this.setState({
            pengumuman : data
          })
      })
    })

  }

  render() {
  const { navigate } = this.props.navigation;
  const { nama,image,news,pengumuman } = this.state
  const pengumuman_filter = pengumuman.filter((data)=> data.number <= 10 )

    return (
      <View style={styles.container}>
          <View style={styles.path1}>
            <Text style={styles.welcome}>Selamat Datang{"\n"}{nama}</Text>
          </View>
          <View style={styles.path2}>
            <TouchableOpacity style={styles.button} onPress={(()=>this.props.navigation.navigate('presensi'))}>
              <View style={styles.imageBackIcon}>
                <Image style={styles.imageIcon} source={require('../component/image/absen.png')}/>
              </View>
              <Text style={styles.textImageIcon}>Presensi</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={(()=>this.props.navigation.navigate('reimburse'))}>
              <View style={styles.imageBackIcon}>
                <Image style={styles.imageIcon} source={require('../component/image/biaya.png')}/>
              </View>
              <Text style={styles.textImageIcon}>Reimburse</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={(()=>this.props.navigation.navigate('perizinan'))}>
              <View style={styles.imageBackIcon}>
                <Image style={styles.imageIcon} source={require('../component/image/perizinan.png')}/>
              </View>
              <Text style={styles.textImageIcon}>Perizinan</Text>
            </TouchableOpacity>
            {renderIf(this.state.role === 'admin' ? true : false)(
              <TouchableOpacity style={styles.button} onPress={(()=>this.props.navigation.navigate('report'))}>
                <View style={styles.imageBackIcon}>
                  <Image style={styles.imageIcon} source={require('../component/image/list.png')}/>
                </View>
                <Text style={styles.textImageIcon}>Report</Text>
              </TouchableOpacity>
            )}
            {renderIf('status' !== 'admin' ? true : false)(
            )}
          </View>
          <View style={styles.imageBack}>
              <TouchableOpacity onPress={(()=> this.props.navigation.navigate('profile') )}>
                <Image style={styles.image} source={{uri: image}}/>
              </TouchableOpacity>
          </View>
        <ScrollView contentContainerStyle={styles.scrolview}>
          <View style={{width:360,marginTop:10}}>
            <View style={{alignItems:'center',jusifyContent:'center'}}>
              <Text style={{fontSize:15,fontWeight:'bold',marginBottom:10,textAlign:'center'}}>
                News
              </Text>
            </View>
              <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow:1,alignItems:'center',paddingBottom:10}}>
                <View style={{width:Dimensions.get('window').width,flexDirection: 'row',flexWrap: 'wrap',justifyContent: 'center'}}>
                  {pengumuman_filter.map((data)=>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('web',({url:data.data.url}))} style={{width:150,height:155,marginTop:10,backgroundColor:'#FFFFFF',marginHorizontal:8,borderRadius:5,elevation: 10,}}>
                      <ImageBackground imageStyle={{borderRadius:5}} source={{uri : data.data.image}} style={{width:150,height:110,}}>
                      </ImageBackground>
                        <Text style={{textAlign:'left',margin:5,height:35}}>{data.data.nama}</Text>
                    </TouchableOpacity>
                  )}
                </View>
              </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

{/*<View style={{width:360}}>
  <View style={{flexDirection:'row'}}>
    <Text style={{marginLeft:20,marginBottom:10}}>
      News
    </Text>
  </View>
    <ScrollView scrollEventThrottle={5} horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{flexGrow : 1,height:120,paddingBottom:20,paddingLeft:5,paddingRight:5}}>
      {news_filter.map((data)=>
        <TouchableOpacity onPress={()=> this.props.navigation.navigate('news',{url: data.data.url})} style={{borderRadius:10,width:200,height:120,backgroundColor:'#000000',marginLeft:10}}>
          <ImageBackground imageStyle={{opacity: .6,borderRadius:10}} source={{uri : data.data.image}} style={{width:200,height:120}}>
            <Text numberOfLines={3} style={{width:180,position:'absolute',color:'#fff',fontWeight:'bold',fontSize:15,paddingTop:60,marginLeft:10}}>{data.data.nama}</Text>
          </ImageBackground>
        </TouchableOpacity>
      )}
    </ScrollView>
</View>*/}

                // <TouchableOpacity>
                //   <Text style={{marginLeft:220,marginTop:10}}>lihat semua</Text>
                // </TouchableOpacity>
