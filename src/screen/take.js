import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  TextInput,
  Picker,
  Text,
  Switch,
  TouchableOpacity,
  Alert,
  Modal,
  ActivityIndicator,
  PermissionsAndroid
} from "react-native";
import { Center } from "@builderx/utils";
import styles from "../component/style/profile"
import firebase from "react-native-firebase"
import store from 'react-native-simple-store';
import ImagePicker from 'react-native-image-picker';
import CompressImage from 'react-native-compress-image';
import ImgToBase64 from 'react-native-image-base64';

const image = "https://www.fote.org.uk/wp-content/uploads/2017/03/profile-icon-300x300.png"

export default class Profile extends Component {

  constructor(props){
    super(props);
    this.state = {
        loading      : false,
        avatarSource : 'https://www.fote.org.uk/wp-content/uploads/2017/03/profile-icon-300x300.png',
        data         : ''
    }
  }

  componentDidMount(){
    store.get('uid').then((res)=>{

      const db = firebase.firestore();
      const base = db.collection("user").doc(res)
      base.onSnapshot( async (doc) => {
        let data = doc.data()
        this.setState({...data})
      })

    })

    store.get('id_perusahaan').then((res)=>{

      const db = firebase.firestore();
      const base = db.collection("perusahaan").doc(res).collection("groub")
      base.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          groub : data
        })
      })

    })

    try {
      PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Cool Photo App Camera Permission',
            message:
              'Cool Photo App needs access to your camera ' +
              'so you can take awesome pictures.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera');
        } else {
          console.log('Camera permission denied');
        }
      } catch (err) {
        console.warn(err);
    }

  }

  render() {
    const { perusahaan,Kodeperusahaan,email,password,groub } = this.state

    return (
      <View style={styles.root}>
        <ScrollView
          style={styles.scrollArea}
          contentContainerStyle={styles.scrollArea_contentContainerStyle}
        >
          <View>
            <TouchableOpacity onPress={this.choosePicture} style={styles.imageBack}>
              <Image
                source={{uri:this.state.avatarSource}}
                resizeMode={"contain"}
                style={styles.image}
              />
              <Text style={styles.textGanti}>Upload Photo</Text>
            </TouchableOpacity>
          </View>

          <Text style={styles.namaLengkap}>Keterangan</Text>
          <TextInput
            value={this.state.Keterangan}
            placeholderTextColor={"rgba(155,155,155,1)"}
            returnKeyType={"next"}
            placeholder="Keterangan"
            onChangeText={(Keterangan)=> this.setState({Keterangan})}
            style={styles.nama}
          />

          <TouchableOpacity style={styles.buttonUpdate} onPress={this.update}>
            <Text style={styles.textUpdate}>Absen</Text>
          </TouchableOpacity>
        </ScrollView>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.loading}>
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <View style={{width:150,height:100,backgroundColor:'#fff',alignItems:'center',justifyContent:'center',elevation:5,borderRadius:10}}>
              <Text style={{marginBottom:5}}>Sedang memproses</Text>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          </View>
        </Modal>
      </View>
    );
  }

  update=()=>{
    if (this.state.avatarSource === image) {
      Alert.alert('Foto lokasi kerja belum diambil','Silahkan foto selfie atau foto lokasi kerja anda')
    } else {
      store.get('uid')
      .then((res)=>{
        this.setState({loading:true})

        CompressImage.createCompressedImage(this.state.avatarSource, 'Compress/Images')
        .then(({uri}) => {
          ImgToBase64.getBase64String(uri)
            .then((base64String) => {
              let formData = new FormData()
              formData.append('key', "b9a600e166895213dbde761c933edf9e")
              formData.append('image', base64String)

              fetch('https://api.imgbb.com/1/upload', {
                method: 'POST',
                headers: {
                      'Content-Type':'multipart/form-data'},
                body: formData
              })
              .then((response)=> response.json())
              .then((responseJson) =>{
                if (responseJson.success === true){
                  const db = firebase.firestore();
                  store.get('groub').then((groub)=>{
                    store.get('nama').then((nama)=>{
                      const take = db.collection("user").doc(res).collection("presensi").doc()
                      take.set({
                        keterangan   : this.state.Keterangan,
                        image        : responseJson.data.url,
                        masuk        : new Date(),
                        keluar       : new Date(),
                        uid          : res,
                        groub        : groub,
                        nama         : nama,
                        id_absen     : take.id
                      })
                      .then(()=>{
                        store.get('id_perusahaan').then((pru)=>{
                          db.collection("perusahaan").doc(pru).collection("Absen_global").doc(take.id).set({
                            keterangan   : this.state.Keterangan,
                            image        : responseJson.data.url,
                            masuk        : new Date(),
                            keluar       : new Date(),
                            uid          : res,
                            groub        : groub,
                            nama         : nama,
                            id_absen     : take.id
                          })
                        })
                        store.save('id_absen', take.id)
                      })
                      
                    })
                  })
                  this.props.navigation.goBack()
                  Alert.alert("Terimakasih","Data Telah di Perbarui")
                  this.setState({loading:false})

                }else if (responseJson.status_code === 400){
                  const db = firebase.firestore();
                  store.get('groub').then((groub)=>{
                    db.collection("user").doc(res).collection("presensi").doc().set({
                      keterangan   : this.state.Keterangan,
                      waktu        : new Date(),
                      uid          : res,
                      groub        : groub
                    })
                    store.get('id_perusahaan').then((pru)=>{
                      db.collection("perusahaan").doc(pru).collection("Absen_global").doc().set({
                        keterangan   : this.state.Keterangan,
                        waktu        : new Date(),
                        uid          : res,
                        groub        : groub
                      })
                    })
                  })
                  this.props.navigation.goBack()
                  Alert.alert("Terimakasih","Telah mengambil absen")
                  this.setState({loading: false})
                }

              })
              .catch((error) =>{
                  console.log(error)
                  Alert.alert('Tidak dapat update','Periksa Koneksi Anda atau ulangi kembali')
                  this.setState({loading: false})
              })
            })
            .catch(err => console.log(err));

          }).catch((err) => {
            console.log(err);
            return Alert.alert('Unable to compress photo, Check the console for full the error message');
        });

      })
    }
  }

  choosePicture = () =>{
      var options = {
          title: 'Pilih Gambar',
          customButtons: [
            { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
          ],
          storageOptions: {
            skipBackup: true,
            path: 'images'
          }
      };

      ImagePicker.launchCamera(options, (response) =>{
          console.log(response)
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
              this.setState({
                srcImg: { uri: response.uri },
                avatarSource: 'file:///' + response.path,
                data : response.data,
                fileName: response.fileName
              });

          }
      });
  };

}