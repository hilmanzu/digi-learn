import React, {Component} from 'react';
import {View,Text,ActivityIndicator} from 'react-native';
import styles from '../component/style/checkin'
import firebase from "react-native-firebase"
import WebView from 'react-native-webview';

class MyWeb extends Component {

  ActivityIndicatorLoadingView() {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <Text style={{marginBottom:20}}>Sedang Mengambil Data</Text>
        <ActivityIndicator
          color="#009688"
          size="large"
          style={styles.ActivityIndicatorStyle}
        />
      </View>
    );
  }


  render() {
  const {params} = this.props.navigation.state;

    return (

    <View style={{flex:1}}>
      <View style={styles.header}>
          <Text style={styles.headertext}>News</Text>
      </View>

      <WebView
        source={{uri: params.url}}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        renderLoading={this.ActivityIndicatorLoadingView}
        startInLoadingState={true}
      />

  </View>

    );

  }
}

export default MyWeb