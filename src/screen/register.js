import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  TextInput,
  Picker,
  Text,
  Switch,
  TouchableOpacity,
  Alert,
  Modal,
  ActivityIndicator
} from "react-native";
import { Center } from "@builderx/utils";
import styles from "../component/style/register"
import firebase from "react-native-firebase"
import store from 'react-native-simple-store';

export default class Regsitrasi extends Component {

  constructor(props){
    super(props);
    this.state = {
        email         :'',
        password      :'',
        Kodeperusahaan:'',
        perusahaan    :[],
        groub         :[],
        choi_groub    :'',
        nama          :'',
        loading: false,
    }
  }

  componentDidMount(){

    const db = firebase.firestore();
    const doc = db.collection("perusahaan")

    doc.onSnapshot(async(querySnapshot)=>{
        var data = []
          querySnapshot.forEach((doc)=>{
            let item = data
            item.push({
              data : doc.data(),
              id   : doc.id
            })
          })
        this.setState({
          perusahaan : data
        })

        var groub_data = data.find((item) => item.data.kode === this.state.Kodeperusahaan)
        
        if (groub_data === undefined) {

          this.setState({
              groub : []
          })

        } else {

          const groub = db.collection("perusahaan").doc(groub_data.id).collection('groub')
          groub.onSnapshot(async(querySnapshot)=>{
          var data = []
            querySnapshot.forEach((doc)=>{
              let item = data
              item.push({
                data : doc.data(),
                id   : doc.id
              })
            })
            this.setState({
              groub : data
            })
          })

        }
        
    })
  }

  render() {
    const { perusahaan,Kodeperusahaan,email,password,groub } = this.state

    return (
      <View style={styles.root}>
        <ScrollView
          style={styles.scrollArea}
          contentContainerStyle={styles.scrollArea_contentContainerStyle}
        >
          <View horizontal>
            <Image
              source={require("../component/image/presensi.png")}
              resizeMode={"contain"}
              style={styles.image}
            />
          </View>
          <TextInput
            placeholder={"Nama Lengkap"}
            placeholderTextColor={"rgba(155,155,155,1)"}
            returnKeyType={"next"}
            onChangeText={(nama)=> this.setState({nama})}
            style={styles.nama}
          />
          <TextInput
            placeholder={"Email"}
            placeholderTextColor={"rgba(155,155,155,1)"}
            keyboardType={"email-address"}
            returnKeyType={"next"}
            onChangeText={(email)=> this.setState({email})}
            style={styles.email}
          />
          <View>
            <TextInput
              placeholder={"Password"}
              placeholderTextColor={"rgba(155,155,155,1)"}
              keyboardType={"default"}
              returnKeyType={"next"}
              secureTextEntry={true}
              onChangeText={(password)=> this.setState({password})}
              style={styles.password}
            />
          </View>
          <View horizontal>
            <TextInput
              placeholder={"Kode Perusahaan"}
              placeholderTextColor={"rgba(155,155,155,1)"}
              keyboardType={"default"}
              returnKeyType={"next"}
              onChangeText={(Kodeperusahaan)=> this.setState({Kodeperusahaan})}
              style={styles.Kodeperusahaan}
            />
          </View>
          <TouchableOpacity style={styles.buttondaftar} onPress={this.register}>
            <Text style={styles.text5}>DAFTAR</Text>
          </TouchableOpacity>
          <View horizontal>
            <Text style={styles.footertext}>
              Presensi Online{"\n"}© 2019 All right reserved. IYOTECH
            </Text>
          </View>
        </ScrollView>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.loading}>
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <View style={{width:150,height:100,backgroundColor:'#fff',alignItems:'center',justifyContent:'center',elevation:5,borderRadius:10}}>
              <Text style={{marginBottom:5}}>Sedang memproses</Text>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          </View>
        </Modal>
      </View>
    );
  }

  register=()=>{
    const { perusahaan,Kodeperusahaan,email,password,nama } = this.state

    var text = this.state.password
    var list = perusahaan.find((item) => item.data.kode === Kodeperusahaan)

    if (this.state.nama == ''){
      alert('Isi Nama Lengkap')
    }else if (this.state.email == ''){
      alert('Isi email anda')
    }else if ( this.state.Kodeperusahaan == '' ){
      alert('Pilih Company Anda')
    }else if ( text.length <= 7){
      alert('Password harus lebih 7 karakter')
    }else if (list === undefined){
      alert('Kode Perusahaan tidak ditemukan')
    }else {
      this.setState({ loading: true });
      const { email, password } = this.state;
      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then((response) => {
          if (response.user.uid){
            this.setState({ loading: false });
            const db = firebase.firestore()
            db.collection('user').doc(response.user.uid).set({
              nama           : nama,
              email          : email,
              id_perusahaan  : list.id,
              perusahaan     : list.data.nama,
              role           : 'karyawan',
              id_user        : response.user.uid,
              image          : 'https://www.fote.org.uk/wp-content/uploads/2017/03/profile-icon-300x300.png'
            })
            .then(()=>{
              store.save('uid', response.user.uid)
              store.save('id_perusahaan',list.id)
              store.save('role','karyawan')
              store.save('nama',nama)
              Alert.alert('Hallo','Selamat anda telah terdaftar')
              this.props.navigation.navigate('menu');
            })
            .catch((error)=>{
              this.setState({ loading: false });
              alert(error)
            })
          }
        })

        .catch((error)=>{
          if ( error.code == 'auth/email-already-in-use' ){
          this.setState({ loading: false });
          Alert.alert('Akun Telah Digunakan','Silahkan daftar dengan email lain')
          }else if ( error.code == 'auth/user-not-found' ){
          this.setState({ loading: false });
          Alert.alert('Akun Tidak ditemukan','Silahkan daftar dengan email lain')
          }else if ( error.code == 'auth/wrong-password'){
          this.setState({ loading: false });
          Alert.alert('password anda salah',)
          }
        })
    }
  }

}