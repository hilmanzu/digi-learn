import React, {Component} from 'react';
import {ActivityIndicator,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../component/style/checkin'
import store from 'react-native-simple-store';
import moment from 'moment'
import 'moment/locale/id'
moment.locale('id')
import firebase from "react-native-firebase"
import { createStackNavigator, createAppContainer } from "react-navigation";
import renderIf from "../component/renderIf"
import take from "./take_out"
const haversine = require('haversine')

class checkout extends Component{
  constructor(props){
    super(props)
    this.state = {
      dashboard:[],
      loading:true
      }
    }

  componentDidMount(){
    store.get('uid')
    .then((res)=>{
      const db = firebase.firestore();
      const base = db.collection("user").doc(res).collection("presensi").orderBy('masuk','desc')
      base.onSnapshot((querySnapshot)=>{
          var data = []
            querySnapshot.forEach((doc)=>{
              let item = data
              item.push({
                data : doc.data(),
                id   : doc.id
              })
            })
          this.setState({
            dashboard : data,loading:false
          })
        })

    })
  }

  render() {
  const { dashboard} = this.state


    return (
      <View style={styles.container}>
        <View style={{width:360,height:100,elevation:5,backgroundColor:'#fff',alignItems:'center',flexDirection:'row'}}>
          <Text style={{flex:1,textAlign:'center',fontSize:50,fontWeight:'bold'}}>{moment().format('h:mm')}</Text>
        </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center'}}>
            {dashboard.map((data)=>
                <View style={{flexDirection:'row',marginTop:20,width:Dimensions.get('window').width}}>
                  <Image style={{width:85,height:85,marginHorizontal:25}} source={{uri:data.data.image}}/>
                  <View>
                    <Text style={{color:'#000000',fontWeight:'bold',fontSize:15}}>{moment(data.data.keluar.toDate()).format('LL')}</Text>
                    {renderIf(moment(data.data.masuk.toDate()).format('LT') === moment(data.data.keluar.toDate()).format('LT') ? true : false)(
                      <Text style={{color:'#000000',fontWeight:'bold',fontSize:15,marginTop:5}}>keluar : {"\n"}jam keluar belum diambil</Text>
                    )}
                    {renderIf(moment(data.data.masuk.toDate()).format('LT') !== moment(data.data.keluar.toDate()).format('LT') ? true : false)(
                      <Text style={{color:'#000000',fontWeight:'bold',fontSize:15,marginTop:5}}>keluar : {"\n"}Pukul {moment(data.data.keluar.toDate()).format('LT')} WIB</Text>
                    )}
                  </View>
                </View>
            )}
          </ScrollView>
          <TouchableOpacity onPress={this.absen} style={{width:180,height:50,borderRadius:10,marginBottom:10,elevation:1,justifyContent:'center',alignItems:'center',backgroundColor:'#420000'}}>
            <View>
              <Text style={{color:'#fff',fontWeight:'bold',fontSize:15}}>CHECKOUT</Text>
            </View>
          </TouchableOpacity>
      </View>
    );
  }

  absen=()=>{
    store.get('uid').then((data) =>{
      store.get('id_absen').then((id_absen)=>{
          store.get('id_perusahaan').then((id_perusahaan)=>{

            const db = firebase.firestore()
            const {dashboard,id,password} = this.state
            const filter = dashboard.filter((item) => moment(item.data.masuk.toDate()).format('LT') !== moment(item.data.keluar.toDate()).format('LT') && moment(item.data.masuk.toDate()).format('MMMM Do YYYY') === moment().format('MMMM Do YYYY'))
            var Id = filter.find((item) => item.data.uid === data)

            if (Id === undefined){
              const db = firebase.firestore()
              db.collection("user").doc(data).collection("presensi").doc(id_absen).update({
                keluar : new Date()
              })
              .then(()=>{
                db.collection("perusahaan").doc(id_perusahaan).collection("Absen_global").doc(id_absen).update({
                  keluar : new Date()
                })
              })
            }else if (Id !== undefined){
              Alert.alert('Opsss','Anda telah melakukan presensi')
            }

          })
      })
    })
  }

}

const AppNavigator = createStackNavigator({
  Home: { screen: checkout },
  take: { screen: take }
},{headerMode   : 'none'});

export default createAppContainer(AppNavigator);
