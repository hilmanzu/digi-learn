import React from 'react';
import {BackHandler,Alert,View, Text,  StyleSheet, Image ,PermissionsAndroid,Platform,ActivityIndicator} from 'react-native';
import store from 'react-native-simple-store';
import firebase from "react-native-firebase"

const logo = 'https://i.ibb.co/b7Yj3vH/presensi.png'

export default class Splashscreen extends React.Component {
    constructor(props){
        super(props)
          this.state = {
            data:'',
          }
      }

    componentDidMount(){

      store.get('uid').then((res)=>{
        if (res) {
          const db = firebase.firestore();
          const base = db.collection("user").doc(res)
          base.onSnapshot( async (doc) => {
            let data = doc.data()
            let id   = doc.id
            store.save('id_perusahaan',data.id_perusahaan)
            store.save('role',data.role)
            store.save('nama',data.nama)
            store.save('image',data.image)
            store.save('groub',data.groub)
            setTimeout(() => this.props.navigation.navigate('menu'), 2000)
          })
        } else {
          setTimeout(() => this.props.navigation.navigate('login'), 2000)
        }

      })

    }

    render() {
    return (
      <View style={styles.container}>
        <Image source={{uri: logo}} style={styles.image} />
        <Text style={{fontWeight:'bold',fontSize:20}}>Presensi Online</Text>
        <Text style={{marginTop:10,marginBottom:50}}>" Bekerja Dimana Saja Jadi Lebih Mudah "</Text>
          <ActivityIndicator
            color={`rgba(0, 163, 192, 1)`}
            size={`large`}
            style={styles.spin}
          />
      </View>
        )
    }
}

const styles = StyleSheet.create ({
   container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  image: {
    width: 120,
    height: 80,
    marginBottom:20
  }
})