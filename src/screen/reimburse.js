import React, {Component} from 'react';
import {TouchableHighlight,ActivityIndicator,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../component/style/checkin'
import store from 'react-native-simple-store';
import moment from 'moment'
import 'moment/locale/id'
moment.locale('id')
import firebase from "react-native-firebase"
var numeral = require('numeral');

export default class checkin extends Component{
  constructor(props){
    super(props)
    this.delete = this.delete.bind(this);
    this.state = {
      dashboard:[],
      loading:true,
      touch:''
      }
    }

  componentDidMount(){
    store.get('uid')
    .then((res)=>{
      const db = firebase.firestore();
      const base = db.collection("user").doc(res).collection("reimburse").orderBy('waktu','desc')
      base.onSnapshot((querySnapshot)=>{
          var data = []
            querySnapshot.forEach((doc)=>{
              let item = data
              item.push({
                data : doc.data(),
                id   : doc.id
              })
            })
          this.setState({
            dashboard : data,loading:false
          })
        })
    })

  }

  render() {
  const { dashboard} = this.state
  const filter = dashboard.filter((data)=> moment(data.data.waktu.toDate()).format('MMMM') === moment().format('MMMM') )
  let totalharga = filter.reduce((a,b)=>{
    a += parseInt(b.data.harga)
    return a
    }, 0)
  const harga = totalharga
  var total = 0

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Reimburse</Text>
        </View>
        <View style={{width:360,height:50,elevation:5,backgroundColor:'#fff',alignItems:'center',flexDirection:'row'}}>
          <Text style={{marginLeft:25,flex:1,textAlign:'left',fontSize:20,fontWeight:'bold'}}>{moment().format('MMMM')}</Text>
          <Text style={{marginRight:25,flex:1,textAlign:'right',fontSize:20,fontWeight:'bold'}}>Rp. {numeral(harga).format('0,0')}</Text>
        </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center'}}>
            {dashboard.map((data)=>
                <TouchableOpacity onPress={(()=>this.delete(data))} style={{flexDirection:'row',marginTop:20,width:Dimensions.get('window').width}} >
                  <Image style={{width:85,height:85,marginHorizontal:25}} source={{uri:data.data.image}}/>
                  <View>
                    <Text style={{width:220,color:'#000000',fontWeight:'bold',fontSize:15,marginTop:5}}>Transaksi : {"\n"}{data.data.keterangan}</Text>
                    <Text style={{color:'#000000',fontWeight:'bold',fontSize:15,marginTop:5}}>Rp. {numeral(data.data.harga).format('0,0')}</Text>
                  </View>
                </TouchableOpacity>
            )}
          </ScrollView>
          <TouchableOpacity onPress={this.absen} style={{width:180,height:50,borderRadius:10,marginBottom:10,elevation:1,justifyContent:'center',alignItems:'center',backgroundColor:'#420000'}}>
            <View>
              <Text style={{color:'#fff',fontWeight:'bold',fontSize:15}}>Upload Resi</Text>
            </View>
          </TouchableOpacity>
      </View>
    );
  }

  absen =() => {
    this.props.navigation.navigate('take_reimburse')
  }

  delete(data){
    const db = firebase.firestore()
    Alert.alert(
      'Opss',
      'Ingin menghapus data ini ?',
      [
        {
          text: 'tidak',
          onPress: () => console.log('Cancel Pressed'),
          style: 'tidak',
        },
        {text: 'OK', onPress: () => 
          store.get('uid').then((uid)=>{
            db.collection("user").doc(uid).collection('reimburse').doc(data.id).delete()
          })
        },
      ],
      {cancelable: false},
    );
  }

}
