import React, {Component} from 'react';
import {TextInput,Modal,TouchableHighlight,ActivityIndicator,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../component/style/checkin'
import store from 'react-native-simple-store';
import moment from 'moment'
import 'moment/locale/id'
moment.locale('id')
import firebase from "react-native-firebase"
var numeral = require('numeral');
import ActionButton from 'react-native-action-button';

export default class checkin extends Component{
  constructor(props){
    super(props)
    this.delete = this.delete.bind(this);
    this.state = {
      dashboard:[],
      loading:false,
      touch:'',
      news:[],
      id:'',
      groub:''
      }
    }

  componentDidMount(){
    store.get('uid').then((uid)=>{
      const db = firebase.firestore();
      const news = db.collection("user").doc(uid).collection("groub").orderBy('waktu','desc')
      news.onSnapshot(async(querySnapshot)=>{
          var data = []
            querySnapshot.forEach((doc)=>{
              let item = data
              item.push({
                data   : doc.data(),
                id     : doc.id,
                number : data.length + 1
              })
            })
          console.log(data)

          this.setState({
            news : data,id:uid
          })
      })
    })
  }

  render() {
  const { news,id,groub} = this.state

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Chating</Text>
        </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center'}}>
            {news.map((data)=>
                <TouchableOpacity onPress={(()=>this.props.navigation.navigate('chat',({chat:data.data.nama}) ))} style={{flexDirection:'row',marginTop:20,width:Dimensions.get('window').width}} >
                  <Image style={{width:55,height:55,marginHorizontal:25,backgroundColor:'#00000020',borderRadius:10}} source={require('../component/image/chat.png')}/>
                  <View>
                    <Text style={{width:220,color:'#000000',fontWeight:'bold',fontSize:20}}>{data.data.nama}</Text>
                    <Text style={{color:'#000000',fontSize:15,marginTop:5}}>Pesan masuk</Text>
                  </View>
                </TouchableOpacity>
            )}
          </ScrollView>
          <ActionButton
            buttonColor="rgba(231,76,60,1)"
            onPress={() => { this.setState({loading:true})}}
          />
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.loading}>
            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
              <View style={{alignItems:'center',justifyContent:'center',width:250,height:200,backgroundColor:'#fff',elevation:5,borderRadius:10}}>
                <Text style={{marginBottom:10}}>Groub Baru</Text>
                <TextInput
                  placeholder={"Nama Group"}
                  placeholderTextColor={"rgba(155,155,155,1)"}
                  keyboardType={"default"}
                  returnKeyType={"next"}
                  onChangeText={(groub)=> this.setState({groub})}
                  style={styles.password}
                />
                <View style={{flexDirection:'row'}}>
                  <TouchableOpacity onPress={this.ok} style={{backgroundColor:'#420000',height:40,width:80,marginRight:10,marginTop:10,alignItems:'center',justifyContent:'center',borderRadius:10}}>
                    <Text style={{color:'#fff'}}>Ok</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => { this.setState({loading:false})}} style={{backgroundColor:'#420000',height:40,width:80,marginLeft:10,marginTop:10,alignItems:'center',justifyContent:'center',borderRadius:10}}>
                    <Text style={{color:'#fff'}}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
      </View>
    );
  }

  absen =() => {
    this.props.navigation.navigate('take_reimburse')
  }

  delete(data){
    const db = firebase.firestore()
    Alert.alert(
      'Opss',
      'Ingin menghapus data ini ?',
      [
        {
          text: 'tidak',
          onPress: () => console.log('Cancel Pressed'),
          style: 'tidak',
        },
        {text: 'OK', onPress: () => 
          store.get('uid').then((uid)=>{
            db.collection("user").doc(uid).collection('reimburse').doc(data.id).delete()
          })
        },
      ],
      {cancelable: false},
    );
  }

  delete=()=>{
    this.props.navigation.navigate('chat')
  }

  ok=()=>{

    store.get('uid').then((uid)=>{

      const db = firebase.firestore()
      const doc = db.collection("user").doc(uid).collection("groub").doc();

      doc.set({
        nama         : this.state.groub,
        waktu        : new Date(),
        id           : doc.id
      })
      .then(()=>{

        this.setState({loading:false})
        store.get('nama').then((nama)=>{
          const chat = db.collection("groub").doc(doc.id);
          const member = db.collection("groub").doc(doc.id).collection("member").doc();
            chat.set({
              nama         : this.state.groub,
              waktu        : new Date(),
              id           : doc.id
            })
            member.set({
              nama         : nama,
              waktu        : new Date(),
              id_user      : uid
            })
        })
        Alert.alert('Berhasil','Groub chat telah dibuat')

      })
      .catch((error)=>{
        Alert.alert('Gagal','Periksa koneksi anda')
      })

    })

  }

}
