import React, {Component} from 'react';
import {Modal,ActivityIndicator,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../component/style/checkin'
import store from 'react-native-simple-store';
import moment from 'moment'
import 'moment/locale/id'
moment.locale('id')
import firebase from "react-native-firebase"
import renderIf from "../component/renderIf"
const haversine = require('haversine')

export default class checkin extends Component{
  constructor(props){
    super(props)
    this.state = {
      dashboard:[],
      loading:true,
      modal:false
      }
    }

  componentDidMount(){
    store.get('uid')
    .then((res)=>{
      store.get('id_perusahaan').then((peru)=>{
        store.get('groub').then((groub)=>{
          const db = firebase.firestore();
          const base = db.collection("perusahaan").doc(peru).collection("Absen_global")
          base.onSnapshot((querySnapshot)=>{
              var data = []
                querySnapshot.forEach((doc)=>{
                  let item = data
                  item.push({
                    data : doc.data(),
                    id   : doc.id
                  })
                })
              this.setState({
                dashboard : data.filter((data)=> data.data.groub === groub && moment(data.data.masuk.toDate()).format('MMMM Do YYYY') === moment().format('MMMM Do YYYY') ),loading:false
              })
            })
        })
      })
    })
  }

  render() {
  const { dashboard} = this.state


    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Report Harian</Text>
        </View>
          <View style={{flexDirection:'row',marginTop:20,width:Dimensions.get('window').width}}>
            <Text style={{marginLeft:10,flex:1,color:'#000000',fontWeight:'bold',fontSize:15}}>Bukti</Text>
            <Text style={{marginLeft:10,flex:3,color:'#000000',fontWeight:'bold',fontSize:15}}>Nama Lengkap</Text>
            <Text style={{flex:1,color:'#000000',fontWeight:'bold',fontSize:15,textAlign:'center'}}>Masuk</Text>
            <Text style={{flex:1,color:'#000000',fontWeight:'bold',fontSize:15,textAlign:'center'}}>Keluar</Text>
          </View>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center'}}>
            {dashboard.map((data)=>
              <View>
                <TouchableOpacity onPress={()=> this.data(data)} style={{marginLeft:10,flexDirection:'row',marginTop:20,width:Dimensions.get('window').width}}>
                  <Image style={{flex:1,width:85,height:40}} source={{uri:data.data.image}}/>
                  <Text style={{marginLeft:10,flex:3,color:'#000000',fontWeight:'bold',fontSize:15}}>{data.data.nama}</Text>
                  <Text style={{flex:1,color:'#000000',fontWeight:'bold',fontSize:15}}>{moment(data.data.masuk.toDate()).format('LT')}</Text>
                  {renderIf(moment(data.data.masuk.toDate()).format('LT') === moment(data.data.keluar.toDate()).format('LT') ? true : false)(
                    <Text style={{flex:1,color:'#000000',fontWeight:'bold',fontSize:15}}>-</Text>
                  )}
                  {renderIf(moment(data.data.masuk.toDate()).format('LT') !== moment(data.data.keluar.toDate()).format('LT') ? true : false)(
                    <Text style={{flex:1,color:'#000000',fontWeight:'bold',fontSize:15}}>{moment(data.data.keluar.toDate()).format('LT')}</Text>
                  )}
                </TouchableOpacity>
              </View>
            )}
          </ScrollView>
          <Modal transparent={true} animationType="slide" visible={this.state.modal}>
            <View style={{alignItems:'center',justifyContent:'center',flex:1}}>
              <Image source={{uri:this.state.image}} style={{width:250,height:200}}/>
              <TouchableOpacity onPress={() => this.setState({modal:false})}>
                <Text style={{padding:10,backgroundColor:'#420000',color:'#fff',borderRadius:10,marginTop:10}}>Tutup</Text>
              </TouchableOpacity>
            </View>
          </Modal>
      </View>
    );
  }

  absen=()=>{
    store.get('uid').then((data) =>{

      const db = firebase.firestore()
      const {dashboard,id,password} = this.state
      const filter = dashboard.filter((item) => moment(item.data.masuk.toDate()).format('LL') == moment().format('Do MMMM YYYY') )
      var Id = filter.find((item) => item.data.uid === data)

      if (Id === undefined){
        this.props.navigation.navigate('take')
      }else if (Id.data.uid === data){
        Alert.alert('Opsss','Anda telah melakukan presensi')
      }

    })
  }

  data=(data)=>{
    this.setState({image:data.data.image,modal:true})
  }

}
