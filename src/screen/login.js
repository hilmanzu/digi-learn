import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  ScrollView,
  Image,
  TextInput,
  Text,
  TouchableOpacity,
  Alert,
  Modal,
  ActivityIndicator
} from "react-native";
import { Center } from "@builderx/utils";
import styles from "../component/style/login"
import firebase from "react-native-firebase"
import store from 'react-native-simple-store';

export default class Login extends Component {

  constructor(props){
    super(props);
    this.state = {
      email:'',
      password:'',
      loading: false,
      key:true,
      view:false,
      hide:true
    }
  }

  render() {
    return (
      <View style={styles.root}>
        <StatusBar hidden={false} style={styles.statusBar} />
        <ScrollView
          style={styles.scrollArea}
          contentContainerStyle={styles.scrollArea_contentContainerStyle}
        >
          <Center horizontal>
            <Image
              source={require("../component/image/presensi.png")}
              resizeMode={"contain"}
              style={styles.image}
            />
            <Text style={{marginTop:190,fontWeight:'bold',fontSize:20}}>Presensi Online</Text>
            <Text style={{marginTop:10}}>" Bekerja Dimana Saja Jadi Lebih Mudah "</Text>
          </Center>
          <Center horizontal>
            <TextInput
              placeholder={"Email"}
              placeholderTextColor={"rgba(155,155,155,1)"}
              keyboardType={"email-address"}
              returnKeyType={"next"}
              onChangeText={(email)=> this.setState({email})}
              style={styles.email}
            />
          </Center>
          <Center>
            <TextInput
              placeholder={"Password"}
              placeholderTextColor={"rgba(155,155,155,1)"}
              keyboardType={"default"}
              returnKeyType={"next"}
              secureTextEntry={true}
              onChangeText={(password)=> this.setState({password})}
              style={styles.password}
            />
          </Center>
          <Text style={styles.resetpassword} onPress={this.forgotPassword}>Reset Password</Text>
          <TouchableOpacity style={styles.masuk} onPress={this.onLogin}>
            <Text style={styles.textMasuk}>MASUK</Text>
          </TouchableOpacity>
          {/*<TouchableOpacity style={styles.daftar} onPress={this.daftar}>
            <Text style={styles.textdaftar}>DAFTAR</Text>
          </TouchableOpacity>*/}
          <Text style={styles.textfooter}>
            Presensi Online{"\n"}© 2019 All right reserved. IYOTECH
          </Text>
        </ScrollView>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.loading}>
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <View style={{width:150,height:100,backgroundColor:'#fff',alignItems:'center',justifyContent:'center',elevation:5,borderRadius:10}}>
              <Text style={{marginBottom:5}}>Sedang memproses</Text>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          </View>
        </Modal>
      </View>
    );
  }

  daftar=()=>{
    this.props.navigation.navigate('register')
  }

  onLogin = () => {
    if (this.state.email == ''){
      Alert.alert('Opss','Isi email anda')
    }else if (this.state.password == ''){
      Alert.alert('Opss','Isi Password anda')
    }else{
      this.setState({ loading: true });
      const { email, password } = this.state;
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then((responseJson) => {
          if (responseJson.user.uid){

              store.save('uid',responseJson.user.uid)
              this.props.navigation.navigate('Splash')

          }

          this.setState({ loading: false });
        })
        .catch((error) => {
         this.setState({ loading: false });
          if ( error.code == 'auth/user-not-found' ){
          Alert.alert('Akun Tidak ditemukan','Silahkan register untuk membuat akun')
          }else if ( error.code == 'auth/wrong-password'){
          Alert.alert('Password anda salah','silahkan periksa kembali password anda')
          }
        });
      }
    }

  forgotPassword = () => {

    if (this.state.email === '') {
      Alert.alert('Opss','Ingin merubah password? silahkan isi email terlebih dahulu')
    } else {
      this.setState({ loading: true })
      firebase.auth().sendPasswordResetEmail(this.state.email)
        .then(function (user) {
          this.setState({ loading: false })
          Alert.alert('Hai','Perubahan password sudah dikirim di email anda, harap cek email anda sekarang')
        })
        .catch(function (e) {
          this.setState({ loading: false })
          Alert.alert('Opss','Email ini belum terdaftar')
        })
    }
  }

}