import {Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "white"
  },
  scrollArea: {
    width: 360,
    backgroundColor: "rgba(255,255,255,1)",
    bottom: 0,
    elevation: 0
  },
  scrollArea_contentContainerStyle: {
    width: 360,
    height: 640,
    alignItems:'center',
    justifyContent:'center'
  },
  image: {
    width: 112.12,
    height: 87.24,
  },
  nama: {
    width: 277,
    height: 50,
    color: "#121212",
    elevation: 0,
    borderRadius: 10,
    borderColor: "rgba(74,144,226,1)",
    borderWidth: 1,
    borderStyle: "solid",
    textAlign: "left",
    marginTop:40
  },
  email: {
    width: 277,
    height: 50,
    color: "#121212",
    elevation: 0,
    borderRadius: 10,
    borderColor: "rgba(74,144,226,1)",
    borderWidth: 1,
    borderStyle: "solid",
    textAlign: "left",
    marginTop:5
  },
  password: {
    width: 277,
    height: 50,
    color: "#121212",
    elevation: 0,
    borderRadius: 10,
    borderColor: "rgba(74,144,226,1)",
    borderWidth: 1,
    borderStyle: "solid",
    textAlign: "left",
    marginTop:5
  },
  Kodeperusahaan: {
    width: 277,
    height: 50,
    color: "#121212",
    elevation: 0,
    borderRadius: 10,
    borderColor: "rgba(74,144,226,1)",
    borderWidth: 1,
    borderStyle: "solid",
    textAlign: "left",
    marginTop:5
  },
  buttondaftar: {
    width: 277,
    height: 50,
    backgroundColor: "rgba(74,144,226,1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    elevation: 0,
    borderRadius: 10,
    borderColor: "rgba(74,144,226,1)",
    marginTop:50
  },
  text5: {
    color: "rgba(255,255,255,1)"
  },
  footertext: {
    color: "#121212",
    fontSize: 14,
    textAlign: "center",
    marginTop:50
  }
});

export default styles