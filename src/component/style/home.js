import {Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';


const styles = StyleSheet.create({

  container: {
    flex: 1,
    alignItems:'center',
    backgroundColor:'#fff',
  },
  scrolview:{
    alignItems:'center'
  },
  path1: {
    width: Dimensions.get('window').width,
    height: 150,
    borderColor: '#420000',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#420000',
    borderBottomEndRadius:10,
    borderBottomStartRadius:10,
    alignItems:'center',
  },
  path2: {
    flexDirection:'row',
    justifyContent:'center',
    marginTop:10,
    width: Dimensions.get('window').width,
    height: 160,
    shadowColor: 'rgba(0, 0, 0, 0.37)',
    shadowOffset: { width: 5, height: 0 },
    shadowRadius: 8,
    borderRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    borderColor: '#420000',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#420000',
  },
  image: {
    width: 110,
    height: 110,
    borderRadius:100,
  },
  imageBack: {
    width: 125,
    height: 125,
    position:'absolute',
    borderRadius:90,
    marginTop:90,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent:'center',
  },
  button:{
    alignItems:'center',
    marginTop:60
  },
  imageIcon: {
    width: 35,
    height: 35,
  },
  imageBackIcon: {
    width: 60,
    height: 60,
    borderRadius:90,
    marginTop:5,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent:'center',
  },
  textImageIcon: {
    width: 80,
    color:'#fff',
    textAlign:'center',
    fontSize:12
  },
  welcome: {
    width: 250,
    color: '#ffffff',
    fontFamily: 'Segoe UI',
    fontSize: 19,
    fontWeight: '700',
    textAlign:'center',
    marginTop:20
  },


})

export default styles