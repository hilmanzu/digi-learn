import {Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "white"
  },
  scrollArea: {
    width: 360,
    backgroundColor: "rgba(255,255,255,1)",
    bottom: 0,
    elevation: 0
  },
  scrollArea_contentContainerStyle: {
    width: 360,
    height: 640,
    alignItems:'center',
  },
  image: {
    width: 150,
    height: 150,
    marginTop:50
  },
  nama: {
    width: 277,
    height: 50,
    color: "#121212",
    elevation: 0,
    borderRadius: 10,
    borderColor: "#420000",
    borderWidth: 1,
    borderStyle: "solid",
    textAlign: "left",
  },
  email: {
    width: 277,
    height: 50,
    color: "#121212",
    elevation: 0,
    borderRadius: 10,
    borderColor: "#420000",
    borderWidth: 1,
    borderStyle: "solid",
    textAlign: "left",
    marginTop:5
  },
  password: {
    width: 277,
    height: 50,
    color: "#121212",
    elevation: 0,
    borderRadius: 10,
    borderColor: "#420000",
    borderWidth: 1,
    borderStyle: "solid",
    textAlign: "left",
    marginTop:5
  },
  Kodeperusahaan: {
    width: 277,
    height: 50,
    color: "#121212",
    elevation: 0,
    borderRadius: 10,
    borderColor: "#420000",
    borderWidth: 1,
    borderStyle: "solid",
    textAlign: "left",
    marginTop:5
  },
  buttonUpdate: {
    width: 277,
    height: 50,
    backgroundColor: "#420000",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    elevation: 0,
    borderRadius: 10,
    borderColor: "#420000",
    marginTop:50
  },
  textUpdate: {
    color: "rgba(255,255,255,1)"
  },
  buttonKeluar: {
    width: 277,
    height: 50,
    backgroundColor: "#420000",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    elevation: 0,
    borderRadius: 10,
    borderColor: "#420000",
    marginTop:5
  },
  textKeluar: {
    color: "rgba(255,255,255,1)"
  },
  footertext: {
    color: "#121212",
    fontSize: 14,
    textAlign: "center",
    marginTop:50
  },
  textGanti:{
    textAlign:'center',marginTop:10
  },
  namaLengkap:{
    marginTop:20,
    marginBottom:10,
    fontWeight:'bold',
    fontSize:15
  }

});

export default styles