import {Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)",
    flexDirection: "row"
  },
  statusBar: {},
  scrollArea: {
    top: 0,
    left: 0,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    right: 0,
    bottom: 0,
    elevation: 0,
  },
  scrollArea_contentContainerStyle: {
    width: 360,
    height: 640
  },
  image: {
    top: 109.59,
    width: 112.12,
    height: 87.24,
    position: "absolute"
  },
  email: {
    top: 300,
    width: 277,
    height: 50,
    color: "#121212",
    position: "absolute",
    elevation: 0,
    borderRadius: 10,
    borderColor: "#420000",
    borderWidth: 1,
    borderStyle: "solid",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowRadius: 0,
    textAlign: "left"
  },
  password: {
    top: 360,
    left: 41.5,
    width: 277,
    height: 50,
    color: "#121212",
    position: "absolute",
    elevation: 0,
    borderRadius: 10,
    borderColor: "#420000",
    borderWidth: 1,
    borderStyle: "solid",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowRadius: 0,
    textAlign: "left"
  },
  resetpassword: {
    top: 420,
    left: 42,
    color: "#121212",
    position: "absolute",
    fontSize: 12
  },
  masuk: {
    top: 450,
    left: "11.53%",
    width: 277,
    height: 50,
    backgroundColor: "#420000",
    position: "absolute",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
    borderRadius: 10,
    borderColor: "#420000",
    borderWidth: 1,
    shadowOffset: {
      height: 0,
      width: 0
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowRadius: 0
  },
  textMasuk: {
    color: "rgba(255,255,255,1)",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  daftar: {
    top: 460,
    left: "11.53%",
    width: 277,
    height: 50,
    backgroundColor: "#420000",
    position: "absolute",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
    borderRadius: 10,
    borderColor: "#420000",
    borderWidth: 1,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowRadius: 0
  },
  textdaftar: {
    color: "rgba(255,255,255,1)"
  },
  text2: {
    top: 356.67,
    left: 240.15,
    color: "#121212",
    position: "absolute",
    fontSize: 12
  },
  textfooter: {
    top: 550,
    left: 71.86,
    color: "#121212",
    position: "absolute",
    fontSize: 14,
    textAlign: "center"
  }
});

export default styles