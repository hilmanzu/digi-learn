import React, {Component} from 'react';
import {WebView,View,Text} from 'react-native';
import styles from './component/style'
import firebase from "react-native-firebase"

class MyWeb extends Component {

  render() {
  const {params} = this.props.navigation.state;

  const Banner = firebase.admob.Banner;
  const AdRequest = firebase.admob.AdRequest;
  const request = new AdRequest();
  request.addKeyword('laptop murah');

    return (

    <View style={{flex:1}}>
	   	<View style={styles.header}>
	        <Text style={styles.headertext}>News</Text>
	    </View>

	    <WebView
	        source={{uri: params.url}}
	    />

	    <Banner
          unitId={'ca-app-pub-8694320270975969/7056513772'}
          size={'SMART_BANNER'}
          request={request.build()}
          onAdLoaded={() => {
            console.log('Advert loaded');
          }}
        />

	</View>

    );

  }
}

export default MyWeb